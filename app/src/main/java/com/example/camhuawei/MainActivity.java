package com.example.camhuawei;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button button;
    TextureView textureView;
    ImageView imageView;

    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270 );
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    private String cameraId;
    CameraDevice cameraDevice;
    CameraCaptureSession cameraCaptureSession;
    CaptureRequest captureRequest;
    CaptureRequest.Builder captureRequestBuilder;

    private Size imageDimension;
    private ImageReader imageReader;
    private File file;
    Handler mBackgroundHandler;
    HandlerThread mBackgroundHandlerThread;
    Handler mainHandler;

    TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(@NonNull SurfaceTexture surface, int width, int height) {
            try {
                openCamera();
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onSurfaceTextureSizeChanged(@NonNull SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(@NonNull SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(@NonNull SurfaceTexture surface) {

        }
    };

    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            cameraDevice = camera;
            try {
                createCameraPreview();
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            cameraDevice.close();
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            cameraDevice.close();
            cameraDevice = null;
        }
    };

    private void createCameraPreview() throws CameraAccessException {
        SurfaceTexture texture = textureView.getSurfaceTexture();
        texture.setDefaultBufferSize( imageDimension.getWidth(), imageDimension.getHeight() );
        Surface surface = new Surface( texture );

        captureRequestBuilder = cameraDevice.createCaptureRequest( CameraDevice.TEMPLATE_PREVIEW );
        captureRequestBuilder.addTarget( surface );

        cameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {
            @Override
            public void onConfigured(@NonNull CameraCaptureSession session) {
                cameraCaptureSession = session;
                try {
                    updatePreview();
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                Toast.makeText( getApplicationContext(), "Config Changed", Toast.LENGTH_LONG ).show();
            }
        }, null);
    }

    private void updatePreview() throws CameraAccessException {
        if ( cameraDevice == null ) {
            return;
        }

        captureRequestBuilder.set( CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        cameraCaptureSession.setRepeatingRequest( captureRequestBuilder.build(), null, mBackgroundHandler );
    }

    private void openCamera() throws CameraAccessException {
        CameraManager manager = ( CameraManager ) getSystemService(Context.CAMERA_SERVICE);
        cameraId = manager.getCameraIdList()[0];

        CameraCharacteristics characteristics = manager.getCameraCharacteristics( cameraId );
        StreamConfigurationMap map = characteristics.get( CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP );

        imageDimension = map.getOutputSizes( SurfaceTexture.class )[0];

        if (ActivityCompat.checkSelfPermission( this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission( this, Manifest.permission.WRITE_EXTERNAL_STORAGE ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions( MainActivity.this, new String[]{
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, 100 );
            return;
        }

        manager.openCamera( cameraId, stateCallback, null );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById( R.id.btn );
        textureView = findViewById( R.id.textureView );
        imageView = findViewById( R.id.imageView );

        mainHandler = new Handler( getMainLooper() );

        textureView.setSurfaceTextureListener( textureListener );
        
        button.setOnClickListener(v -> {
            try {
                takePicture();
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        });
    }

    private void takePicture() throws CameraAccessException {
        if ( cameraDevice == null ) return;

        CameraManager manager = ( CameraManager ) getSystemService( Context.CAMERA_SERVICE );

        CameraCharacteristics characteristics = manager.getCameraCharacteristics( cameraDevice.getId() );
        Size[] jpegSizes = null;

        jpegSizes = characteristics.get( CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP ).getOutputSizes(ImageFormat.JPEG);

        int width = 640;
        int height = 480;

        if ( jpegSizes != null && jpegSizes.length > 0 ) {
            width = jpegSizes[0].getWidth();
            height = jpegSizes[0].getHeight();
        }

        ImageReader reader = ImageReader.newInstance( width, height, ImageFormat.JPEG, 1 );
        List<Surface> outputSurfaces = new ArrayList<>(2);
        outputSurfaces.add(reader.getSurface());
        outputSurfaces.add( new Surface( textureView.getSurfaceTexture() ) );

        final CaptureRequest.Builder builder = cameraDevice.createCaptureRequest( CameraDevice.TEMPLATE_STILL_CAPTURE );
        builder.addTarget( reader.getSurface() );
        builder.set( CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO );
        builder.set( CaptureRequest.CONTROL_AE_MODE, CameraMetadata.CONTROL_AE_MODE_OFF );

        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        builder.set( CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get( rotation ) );

        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();

        file = new File( getExternalFilesDir(null) + "/" + ts + ".jpg" );

        ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
            @Override
            public void onImageAvailable(ImageReader reader) {
                Image image = reader.acquireLatestImage();
                if ( image != null ) {
                    System.out.println( "not null" );
                }
                else System.out.println( "null" );
                ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                byte[] bytes = new byte[ buffer.capacity() ];
                buffer.get( bytes );
                Bitmap bitmap = BitmapFactory.decodeByteArray( bytes, 0, bytes.length, null );
                mainHandler.post( () -> imageView.setImageBitmap(Bitmap.createScaledBitmap( bitmap, 300, 300, false ) ) );

                try {
                    save( bytes );
                } catch (IOException e) {
                    e.printStackTrace();
                }
                finally {
                    image.close();
                }
            }

            private void save( byte[] bytes ) throws IOException {
                OutputStream outputStream = new FileOutputStream( file );
                outputStream.write( bytes );
                outputStream.close();
            }
        };

        reader.setOnImageAvailableListener( readerListener, mBackgroundHandler );

        final CameraCaptureSession.CaptureCallback captureListener = new CameraCaptureSession.CaptureCallback() {
            @Override
            public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                super.onCaptureCompleted(session, request, result);
                Toast.makeText( getApplicationContext(), "SAVED", Toast.LENGTH_LONG ).show();
                try {
                    createCameraPreview();
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }
            }
        };

        cameraDevice.createCaptureSession(outputSurfaces, new CameraCaptureSession.StateCallback() {
            @Override
            public void onConfigured(@NonNull CameraCaptureSession session) {
                try {
                    session.capture( builder.build(), captureListener, mBackgroundHandler );
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onConfigureFailed(@NonNull CameraCaptureSession session) {

            }
        }, mBackgroundHandler);
    }

    @Override
    protected void onResume() {
        super.onResume();

        startBackgroundThread();
        if ( textureView.isAvailable() ) {
            try {
                openCamera();
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
        else {
            textureView.setSurfaceTextureListener( textureListener );
        }
    }

    private void startBackgroundThread() {
        mBackgroundHandlerThread = new HandlerThread( "camera" );
        mBackgroundHandlerThread.start();
        mBackgroundHandler = new Handler( mBackgroundHandlerThread.getLooper() );
    }

    @Override
    protected void onPause() {
        try {
            stopBackgroundThred();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        super.onPause();
    }

    private void stopBackgroundThred() throws InterruptedException {
        mBackgroundHandlerThread.quitSafely();

        mBackgroundHandlerThread.join();
        mBackgroundHandlerThread = null;
        mBackgroundHandler = null;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if ( grantResults[0] == PackageManager.PERMISSION_DENIED ) {
            Toast.makeText( getApplicationContext(), "permission necessary", Toast.LENGTH_LONG ).show();
        }
    }
}